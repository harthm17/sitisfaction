# SITisfaction

Diplomarbeit der HTBLA Kaindorf aus dem Schuljahr 2021/22

## Mitglieder & Aufgabenbereich


| Mitglied | Aufgabenbereich | 
| ---- | ----- 
| Adam Mike | Mechanik und Konstruktion
| Harrer Thomas | Elektrotechnik
| Pust Thomas | Softwareentwicklung - Android App
